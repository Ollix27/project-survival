// priority: 0

settings.logAddedRecipes = true
settings.logRemovedRecipes = true
settings.logSkippedRecipes = false
settings.logErroringRecipes = true

console.info('Hello, World! (You will see this line every time server resources reload)')

onEvent('recipes', event => {
	event.remove({ output: 'minecraft:wooden_sword' })
	event.remove({ output: 'minecraft:wooden_pickaxe' })
	event.remove({ output: 'minecraft:wooden_axe' })
	event.remove({ output: 'minecraft:wooden_hoe' })
	event.remove({ output: 'minecraft:wooden_shovel' })
	event.remove({ output: 'minecraft:stone_sword' })
	event.remove({ output: 'minecraft:stone_pickaxe' })
	event.remove({ output: 'minecraft:stone_shovel' })
	event.remove({ output: 'minecraft:stone_hoe' })
	event.remove({ output: 'minecraft:stone_axe' })
	event.remove({ output: 'minecraft:golden_sword' })
	event.remove({ output: 'minecraft:golden_shovel' })
	event.remove({ output: 'minecraft:golden_pickaxe' })
	event.remove({ output: 'minecraft:golden_hoe' })
	event.remove({ output: 'minecraft:golden_axe' })
	event.remove({ output: 'minecraft:iron_sword' })
	event.remove({ output: 'minecraft:iron_hoe' })
	event.remove({ output: 'minecraft:iron_shovel' })
	event.remove({ output: 'minecraft:iron_axe' })
	event.remove({ output: 'minecraft:iron_pickaxe' })
	event.remove({ output: 'minecraft:diamond_sword' })
	event.remove({ output: 'minecraft:diamond_shovel' })
	event.remove({ output: 'minecraft:diamond_pickaxe' })
	event.remove({ output: 'minecraft:diamond_axe' })
	event.remove({ output: 'minecraft:netherite_sword' })
	event.remove({ output: 'minecraft:netherite_axe' })
	event.remove({ output: 'minecraft:netherite_pickaxe' })
	event.remove({ output: 'minecraft:netherite_shovel' })
})

onEvent('item.tags', event => {
	// Get the #forge:cobblestone tag collection and add Diamond Ore to it
	// event.get('forge:cobblestone').add('minecraft:diamond_ore')

	// Get the #forge:cobblestone tag collection and remove Mossy Cobblestone from it
	// event.get('forge:cobblestone').remove('minecraft:mossy_cobblestone')
})